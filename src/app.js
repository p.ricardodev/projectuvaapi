new Vue({
el:'#app',
data(){
    return {
      compras:[],
    }
  },

async mounted() {
	await this.getPedidos();
},

methods:{
	async getPedidos() {

	          const url='http://localhost/projectUVA/public/api/pedidos';  
	          const response = await axios.get(url);
	            try {      
	                  this.compras = response.data;
	               
	            } catch(error) {
	              //Component Element - loading da pagina
	              console.log(error);
	              //this.fullscreenLoading = false;
	            }
	        }
	 }

});