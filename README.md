## Descrição
O micro serviço consome uma API com dados das vendas de uma empresa varejista de roupas.
Conceitos e aplicação de reuso e promisses foram aplicados no projeto.

## Tecnologias

Vue.js 2x <a href="https://br.vuejs.org/v2/guide/installation.html"><img src="https://img.shields.io/static/v1?label=vue.js 2x&message=framework&color=white&style=for-the-badge&logo=vue.js"/></a>

Element io <a href="https://element.eleme.io/#/en-US"><img src="https://img.shields.io/static/v1?label=element.io&message=framework&color=blue&style=for-the-badge&logo=elemente"/></a>

Axios <a href="https://github.com/axios/axios"><img src="https://img.shields.io/static/v1?label=AXIOS&message=Biblioteca&color=blue&style=for-the-badge&logo=javascript"/></a>

